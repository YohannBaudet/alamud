# Jules Grandjean, IUT Informatique Orléans

from .event import Event2

class ScrubEvent(Event2):
    NAME = "scrub"

    def perform(self):
        if not self.object.has_prop("scrubable"):
            self.fail()
            return self.inform("scrub.failed")
        self.inform("scrub")