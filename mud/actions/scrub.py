# Jules Grandjean, IUT Informatique Orléans

from .action import Action2
from mud.events import ScrubEvent

class ScrubAction(Action2):
    EVENT = ScrubEvent
    ACTION = "scrub"
    RESOLVE_OBJECT = "resolve_for_use"